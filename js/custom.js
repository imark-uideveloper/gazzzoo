$(document).ready(function () {

    //Wow
    wow = new WOW({

        mobile: false // default
    })
    wow.init();

    // Header Fixed
    var h = jQuery(".main-menu-wrap");
    jQuery(window).scroll(function () {
        var windowpos = jQuery(window).scrollTop(),
            bannerHeight = jQuery('.banner-wrapper').outerHeight();
        //        HomeBanner = jQuery('.display_cell').outerHeight();
        console.log(bannerHeight);
        if (windowpos > bannerHeight) {
            h.addClass("fixed-me animated fadeInDown");
        } else {
            h.removeClass("fixed-me animated fadeInDown");
        }
        if ($(window).width() < 767) {

            if (windowpos > 5) {
                h.addClass("fixed-me animated fadeInDown");
            } else {
                h.removeClass("fixed-me animated fadeInDown");
            }
        }

    });


    //Page Loading Animation

    jQuery('.post').addClass("visible").viewportChecker({
        classToAdd: 'visible animated fadeInDown', // Class to add to the elements when they are visible
        offset: 100
    });
});

//Textarea 

$(function (jQuery) {
    $('.firstCap').on('keypress', function (event) {
        var $this = $(this),
            thisVal = $this.val(),
            FLC = thisVal.slice(0, 1).toUpperCase();
        con = thisVal.slice(1, thisVal.length);
        $(this).val(FLC + con);
    });
});

jQuery(document).ready(function ($) {
    jQuery('.what-we-do-list a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                jQuery("nav li a").removeClass("active");
                jQuery(this).addClass('active');
                jQuery('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});

// ===== Scroll to Top ==== 
$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) { // If page is scrolled more than 50px
        $('#return-to-top').fadeIn(200); // Fade in the arrow
    } else {
        $('#return-to-top').fadeOut(200); // Else fade out the arrow
    }
});
$('#return-to-top').click(function () { // When arrow is clicked
    $('body,html').animate({
        scrollTop: 0 // Scroll to top of body
    }, 500);
});
